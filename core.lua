--register this file with Ace Libraries
local DSM = select(2, ...)
DSM = LibStub("AceAddon-3.0"):NewAddon(DSM, "DeSpamMaster", "AceConsole-3.0")


-- List of filter functions.
-- The functions get passed two parameters: msg, player
-- They should return true if the lines should be filtered, false otherwise.
local filterFunctions = {
	function(msg)
		msg = gsub(msg, " ", "")
		
		if strfind(msg, "www%p.*%pcom") and (strfind(msg, "usd") or strfind(msg, "welcometo") or strfind(msg, "cheapestgold")) then
			return true
		end
		
		if select(2, gsub(msg, strchar(208), "")) > 0 then
			return true
		end
		
		local count = 0
		for i=1, #msg do
			if strbyte(strsub(msg, i, i)) > 127 then
				count = count + 1
			end
		end
		
		return count > 5
	end,

	function(msg)
		return strfind(msg, "thunderfury, blessed blade of the windseeker")
	end,

	function(msg)
		return strfind(msg, "anal ")
	end,
	
	function(msg)
		return strfind(msg, "skype")
	end,
	
	function(msg, player)
		local cutoff = time() - DSM.db.factionrealm.duplicateThreshold
		for i, v in pairs(DSM.db.factionrealm.history) do
			if v < cutoff then
				DSM.db.factionrealm.history[i] = nil
			end
		end

		local key = msg.."\001"..player
		if DSM.db.factionrealm.history[key] then
			return true
		end
		DSM.db.factionrealm.history[key] = time()
	end,
	
	function(_, _, tag)
		if tag == "DND" then
			return true
		end
	end,
	
	function(msg)
		local _, num1 = gsub(msg, "{", "")
		local _, num2 = gsub(msg, "}", "")
		if num1 == num2 and num1 > 2 then
			return true
		end
	end,
}


function DSM:OnEnable()
	local savedDBDefaults = {
		factionrealm = {
			duplicateThreshold = 300,
			history = {},
		},
	}
	DSM.db = LibStub:GetLibrary("AceDB-3.0"):New("DeSpamMasterDB", savedDBDefaults, true)
	
	local prevLineID, currResult
	local function FilterMessage(_,_,msg,player,_,_,_,tag,chanID,_,_,_,lineID)
		if lineID == prevLineID then
			return currResult
		else
			prevLineID = lineID
			currResult = nil
			msg = strlower(msg:trim()) -- trim space and make message all lower case
			
			-- Don't filter the player / friends / guildmates
			if not CanComplainChat(player) or UnitIsInMyGuild(player) then
				return
			end
			
			-- Iterate through all the filter functions
			for _, filterFunc in ipairs(filterFunctions) do
				if filterFunc(msg, player, tag) then
					currResult = true
					break
				end
			end
			
			if currResult then
				for i=1, NUM_CHAT_WINDOWS do
					local name = GetChatWindowInfo(i)
					if name and strlower(name) == "despammaster" then
						DSM:Printf(_G["ChatFrame"..i], "[%s]: %s", player, msg)
					end
				end
				return true
			end
		end
	end
	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", FilterMessage)
	DSM:RegisterChatCommand("dsm", "SlashCommandHandler")
end

function DSM:SlashCommandHandler(args)
	local args = {(" "):split(args)}
	
	if args[1] == "threshold" and not args[2] then
		DSM:Printf("Current threshold set to %d seconds.", DSM.db.factionrealm.duplicateThreshold)
	elseif args[1] == "threshold" and tonumber(args[2]) then
		DSM.db.factionrealm.duplicateThreshold = tonumber(args[2])
		DSM:Printf("Duplicate threshold set to %d seconds.", tonumber(args[2]))
	elseif args[1] == "reset" then
		DSM.db:ResetDB()
		DSM:Print("Settings reset.")
	else
		foreach(args, print)
		DSM:Print("DeSpamMaster Slash Commands:")
		print("'/dsm' - Prints this list of slash commands") 
		print("'/dsm threshold' - Prints the current duplicate threshold in seconds.")
		print("'/dsm threshold #' - Sets the duplicate threshold to a given number of seconds.")
		print("'/dsm reset' - Resets all DSM settings to their default values.")
	end
end