## Interface: 50200
## Title: DeSpamMaster
## Notes: Does basic general / trade chat filtering of gold spam. 
## Author: Sapu94
## Version: v1.0
## SavedVariables: DeSpamMasterDB
## OptionalDeps: Ace3

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceDB-3.0\AceDB-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml

core.lua